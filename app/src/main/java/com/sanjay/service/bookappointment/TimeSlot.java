
package com.sanjay.service.bookappointment;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TimeSlot {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("available")
    @Expose
    private String available;
    @SerializedName("discount")
    @Expose
    private String discount;
    @SerializedName("c_start_time")
    @Expose
    private String cStartTime;
    @SerializedName("c_end_time")
    @Expose
    private String cEndTime;
    @SerializedName("slot_id")
    @Expose
    private String slotId;
    @SerializedName("block")
    @Expose
    private String block;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAvailable() {
        return available;
    }

    public void setAvailable(String available) {
        this.available = available;
    }

    public String getDiscount() {
        return discount;
    }

    public void setDiscount(String discount) {
        this.discount = discount;
    }

    public String getcStartTime() {
        return cStartTime;
    }

    public void setcStartTime(String cStartTime) {
        this.cStartTime = cStartTime;
    }

    public String getcEndTime() {
        return cEndTime;
    }

    public void setcEndTime(String cEndTime) {
        this.cEndTime = cEndTime;
    }

    public String getSlotId() {
        return slotId;
    }

    public void setSlotId(String slotId) {
        this.slotId = slotId;
    }

    public String getBlock() {
        return block;
    }

    public void setBlock(String block) {
        this.block = block;
    }
}
