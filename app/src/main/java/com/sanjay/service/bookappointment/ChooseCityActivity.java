package com.sanjay.service.bookappointment;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ProgressBar;


import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.jaredrummler.materialspinner.MaterialSpinner;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;

import cn.pedant.SweetAlert.SweetAlertDialog;


public class ChooseCityActivity extends AppCompatActivity implements View.OnClickListener
{
    Button btnNext;
    EditText edtDate;
    MaterialSpinner materailSpinnerRegion,materailSpinnerSubRegion;
    ProgressDialog progressDialog;


    String selectedRegion,selectedSubregion,region,subregion,dte,docID;
    ArrayList<String> arryRegion;
    ArrayList<String> arrySubRegion;
    DatabaseReference myRef;


    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choose_city);

        Bundle bundle = getIntent().getExtras();
        docID = bundle.getString("docID");

        myRef = FirebaseDatabase.getInstance().getReference();
        edtDate = (EditText) findViewById(R.id.activity_ed_date);
        materailSpinnerRegion = findViewById(R.id.spinner_city);
        materailSpinnerSubRegion = findViewById(R.id.spinner_subcity);
        btnNext = findViewById(R.id.btn_next);


        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Loading");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setProgress(0);


        arryRegion = new ArrayList<>();
        arrySubRegion = new ArrayList<>();


       // region add
        arryRegion.clear();
        myRef.child("Admin").child("City").addListenerForSingleValueEvent(new ValueEventListener()
        {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot)
            {
                for(DataSnapshot childSnapshot: snapshot.getChildren())
                {

                  region = childSnapshot.getKey();
                  arryRegion.add(region);
                }
            }
            @Override
            public void onCancelled(@NonNull DatabaseError error)
            {

            }
        });
        materailSpinnerRegion.setItems(arryRegion);


        arrySubRegion.clear();
       // add sub region
         materailSpinnerRegion.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener()
         {
             @Override
             public void onItemSelected(MaterialSpinner view, int position, long id, Object item)
             {
                 selectedRegion = item.toString();

                 myRef.child("Admin").child("City").child(selectedRegion).addListenerForSingleValueEvent(new ValueEventListener()
                 {
                     @Override
                     public void onDataChange(@NonNull DataSnapshot snapshot)
                     {
                         arrySubRegion.clear();
                         for(DataSnapshot childSnapshot: snapshot.getChildren())
                         {
                             subregion = childSnapshot.getKey();
                             arrySubRegion.add(subregion);
                         }
                     }
                     @Override
                     public void onCancelled(@NonNull DatabaseError error)
                     {

                     }
                 });

             }
         });
         materailSpinnerSubRegion.setItems(arrySubRegion);

         edtDate.setOnClickListener(this);
         btnNext.setOnClickListener(this);


    }


    @Override
    public void onClick(View v)
    {
       if(v == edtDate)
       {
           AppointmentDte();
       }
       else if(v == btnNext)
       {
             FeatchDetails();
       }
    }

    private void AppointmentDte()
    {
        Calendar c = Calendar.getInstance();
        int mYear = c.get(Calendar.YEAR); // current year
        int mMonth = c.get(Calendar.MONTH); // current month
        int mDay = c.get(Calendar.DAY_OF_MONTH); // current day

        DatePickerDialog datePickerDialog = new DatePickerDialog(ChooseCityActivity.this, new DatePickerDialog.OnDateSetListener()
        {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth)
            {
                String monthString = String.valueOf(monthOfYear+1);
                if (monthString.length() == 1)
                {
                    monthString = "0" + monthString;
                }

                String dayString = String.valueOf(dayOfMonth);
                if (dayString.length() == 1)
                {
                    dayString = "0" + dayString;
                }
                edtDate.setText(year + "-" + (monthString) + "-" + dayString);
            }
        }, mYear, mMonth, mDay);


        // get 4 month before date
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.MONTH, 0);

        datePickerDialog.getDatePicker().setMinDate(calendar.getTimeInMillis()-1000);
        datePickerDialog.show();
    }

    private void FeatchDetails()
    {

        selectedRegion = materailSpinnerRegion.getText().toString();
        selectedSubregion = materailSpinnerSubRegion.getText().toString();
        dte = edtDate.getText().toString();

        if(selectedRegion.isEmpty())
        {
            new SweetAlertDialog(this, SweetAlertDialog.ERROR_TYPE)
                    .setTitleText("Error")
                    .setContentText("Selected Region")
                    .show();
        }
        else if(selectedSubregion.isEmpty())
        {
            new SweetAlertDialog(this, SweetAlertDialog.ERROR_TYPE)
                    .setTitleText("Error")
                    .setContentText("Selected SubRegion")
                    .show();
        }
        else if(dte.isEmpty())
        {
            new SweetAlertDialog(this, SweetAlertDialog.ERROR_TYPE)
                    .setTitleText("Error")
                    .setContentText("choose date")
                    .show();
        }
        else
        {
            Intent intent = new Intent(ChooseCityActivity.this, SlotActivity.class);
            intent.putExtra("selectedRegion",selectedRegion);
            intent.putExtra("selectedSubregion",selectedSubregion);
            intent.putExtra("dte",dte);
            intent.putExtra("docID",docID);
            startActivity(intent);

        }

    }


}