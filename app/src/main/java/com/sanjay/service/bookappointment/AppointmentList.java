package com.sanjay.service.bookappointment;

import java.util.List;

public class AppointmentList
{
    public String documentStatus,contactPerson,executionerDtails,bioDate,bioStartTime,bioEndTime,document,attendessize;
    public List<AttendeesList> attList;
    public Object attendesObject;

    public AppointmentList(String documentStatus, String contactPerson, String executionerDtails, String bioDate, String bioStartTime, String bioEndTime,String attendessize ,List<AttendeesList> attList,String document,Object attendesObject) {
        this.documentStatus = documentStatus;
        this.contactPerson = contactPerson;
        this.executionerDtails = executionerDtails;
        this.bioDate = bioDate;
        this.bioStartTime = bioStartTime;
        this.bioEndTime = bioEndTime;
        this.attendessize = attendessize;
        this.attList = attList;
        this.document = document;
        this.attendesObject = attendesObject;
    }

    public AppointmentList(String documentStatus, String contactPerson, String executionerDtails, String bioDate, String bioStartTime, String bioEndTime, String document) {
        this.documentStatus = documentStatus;
        this.contactPerson = contactPerson;
        this.executionerDtails = executionerDtails;
        this.bioDate = bioDate;
        this.bioStartTime = bioStartTime;
        this.bioEndTime = bioEndTime;
        this.document = document;
    }



    public String getDocumentStatus() {
        return documentStatus;
    }

    public void setDocumentStatus(String documentStatus) {
        this.documentStatus = documentStatus;
    }

    public String getContactPerson() {
        return contactPerson;
    }

    public void setContactPerson(String contactPerson) {
        this.contactPerson = contactPerson;
    }

    public String getExecutionerDtails() {
        return executionerDtails;
    }

    public void setExecutionerDtails(String executionerDtails) {
        this.executionerDtails = executionerDtails;
    }

    public String getBioDate() {
        return bioDate;
    }

    public void setBioDate(String bioDate) {
        this.bioDate = bioDate;
    }

    public String getBioStartTime() {
        return bioStartTime;
    }

    public void setBioStartTime(String bioStartTime) {
        this.bioStartTime = bioStartTime;
    }

    public String getBioEndTime() {
        return bioEndTime;
    }

    public void setBioEndTime(String bioEndTime) {
        this.bioEndTime = bioEndTime;
    }

    public List<AttendeesList> getAttList() {
        return attList;
    }

    public void setAttList(List<AttendeesList> attList) {
        this.attList = attList;
    }

    public String getDocument() {
        return document;
    }

    public void setDocument(String document) {
        this.document = document;
    }

    public String getAttendessize() {
        return attendessize;
    }

    public void setAttendessize(String attendessize) {
        this.attendessize = attendessize;
    }

    public Object getAttendesObject() {
        return attendesObject;
    }

    public void setAttendesObject(Object attendesObject) {
        this.attendesObject = attendesObject;
    }
}
