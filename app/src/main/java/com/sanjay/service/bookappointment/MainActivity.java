package com.sanjay.service.bookappointment;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import cn.pedant.SweetAlert.SweetAlertDialog;

import static com.sanjay.service.bookappointment.BasicDetailsReq.APPOINTMENTURL;
import static com.sanjay.service.bookappointment.BasicDetailsReq.getCancelAppointmrnt;
import static com.sanjay.service.bookappointment.BasicDetailsReq.get_auth_token;

public class MainActivity extends AppCompatActivity implements View.OnClickListener
{
    Button btnBookApp,btnBookAppDetilas,btnBookAppCancel;
    ProgressDialog progressDialog;

    String docID;
    BasicDetailsReq response;


    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

      // Bundle bundle = getIntent().getExtras();
     //  docID = bundle.getString("docID",docID);

        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Loading");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setProgress(0);

        response = new BasicDetailsReq();

        docID =	"A_94187";//"A_94129";//"A_94132";//"A_94129";//"A_94017";//	A_94132

       btnBookApp = findViewById(R.id.btn_bappointment);
       btnBookAppDetilas = findViewById(R.id.btn_bappointmentdetails);


       btnBookApp.setOnClickListener(this);
       btnBookAppDetilas.setOnClickListener(this);

    }

    @Override
    public void onClick(View v)
    {
        if(v == btnBookApp)
        {
           Intent intent = new Intent(MainActivity.this,ChooseCityActivity.class);
           intent.putExtra("docID",docID);
           startActivity(intent);
        }
        else if(v == btnBookAppDetilas)
        {
            Intent intent = new Intent(MainActivity.this, AppointmentDetailsActivity.class);
            intent.putExtra("docID",docID);
            startActivity(intent);
        }
    }


}