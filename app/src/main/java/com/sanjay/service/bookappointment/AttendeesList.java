package com.sanjay.service.bookappointment;

public class AttendeesList
{
    public String name,mobile;

    public AttendeesList(String name, String mobile)
    {
        this.name = name;
        this.mobile = mobile;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }
}

