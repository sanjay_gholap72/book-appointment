package com.sanjay.service.bookappointment;



import android.util.Log;

import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

public class BasicDetailsReq
{
    OkHttpClient client = new OkHttpClient();
    public static final MediaType JSON = MediaType.parse("application/json; charset=UTF8");
    public static final String getTimeSlot = "http://52.33.203.208/plus/api/v1/get_time_slot?" ;
    public static final String getAppointmentDetails = "http://dev.anulom.com/plus/api/v1/create_appointments/fetch_appointment_details?";
    public static final String getCancelAppointmrnt = "http://dev.anulom.com/plus/api/v1/create_appointments/appointment_cancel";
    public static final String getReschedule = "http://dev.anulom.com/plus/api/v1/create_appointments/appointment_reschedule";
    public static final String  mID ="Anulom";
    public static final String  get_auth_token ="DOtUBMhv5pk51tl0D37uBcezq85cXNN7hZQ7";
    public static final String APPOINTMENTURL = "http://52.33.203.208/plus/api/v1/appointment_data/book";


    public String doGetRequest(String url) throws IOException
    {
        Log.wtf("url",url);
        Request request = new Request.Builder()
                .url(url)
                .build();
        Response response = client.newCall(request).execute();
        return response.body().string();
    }

    public String doPostRequest(String url, String json) throws IOException
    {

        RequestBody body = RequestBody.create(JSON, (json));
        Request request = new Request.Builder()
                .url(url)
                .post(body)
                .build();


        client.setConnectTimeout(60, TimeUnit.SECONDS);  //Connect timeout
        client.setReadTimeout(60, TimeUnit.SECONDS);    //Socket timeout
        Response response = client.newCall(request).execute();
        return response.body().string();
    }
}
