package com.sanjay.service.bookappointment;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.jaredrummler.materialspinner.MaterialSpinner;

import java.util.ArrayList;
import java.util.Calendar;

import cn.pedant.SweetAlert.SweetAlertDialog;

public class RescheduleCityActivity extends AppCompatActivity implements View.OnClickListener
{
    String region_name,slot_id,cityName,app_id,appDte,docID,contact_person,address;
    String newRegionName,newCityName,newDate;
    String flag1 = "1";
    String flag2 = "1";
    String flag3 = "1";


    ArrayList<String> countAttendeesName,countAttendeesEmail,countAttendeesMobile;

    Button btnNext;
    EditText edtDate;
    MaterialSpinner materailSpinnerRegion,materailSpinnerSubRegion;
    ProgressDialog progressDialog;

    String selectedRegion,selectedSubregion,region,subregion,dte;
    ArrayList<String> arryRegion;
    ArrayList<String> arrySubRegion;
    DatabaseReference myRef;


    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reschedule_city);

        Bundle bundle = getIntent().getExtras();
        countAttendeesName = bundle.getStringArrayList("countAttendeesName");
        countAttendeesEmail = bundle.getStringArrayList("countAttendeesEmail");
        countAttendeesMobile = bundle.getStringArrayList("countAttendeesMobile");
        region_name = bundle.getString("region_name");
        cityName = bundle.getString("cityName");
        appDte = bundle.getString("appDte");
        app_id = bundle.getString("app_id");
        docID = bundle.getString("docID");
        contact_person = bundle.getString("contact_person");
        address= bundle.getString("address");
        slot_id = bundle.getString("slot_id");

        myRef = FirebaseDatabase.getInstance().getReference();
        edtDate = (EditText) findViewById(R.id.activity_ed_date);
        materailSpinnerRegion = findViewById(R.id.spinner_city);
        materailSpinnerSubRegion = findViewById(R.id.spinner_subcity);
        btnNext = findViewById(R.id.btn_next);

        Log.wtf("countAttendeesName", String.valueOf(countAttendeesName.size()));

        for(int i=0;i<countAttendeesName.size();i++)
        {
            Log.wtf("countAttendeesName",countAttendeesName.get(i));
            Log.wtf("countAttendeesEmail",countAttendeesEmail.get(i));
            Log.wtf("countAttendeesMobile",countAttendeesMobile.get(i));
        }


        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Loading");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setProgress(0);

        arryRegion = new ArrayList<>();
        arrySubRegion = new ArrayList<>();

        // region add
        arryRegion.clear();
        myRef.child("Admin").child("City").addListenerForSingleValueEvent(new ValueEventListener()
        {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot)
            {
                for(DataSnapshot childSnapshot: snapshot.getChildren())
                {

                    region = childSnapshot.getKey();
                    arryRegion.add(region);
                }
            }
            @Override
            public void onCancelled(@NonNull DatabaseError error)
            {

            }
        });

        materailSpinnerRegion.setItems(arryRegion);

        arrySubRegion.clear();
        // add sub region
        materailSpinnerRegion.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener()
        {
            @Override
            public void onItemSelected(MaterialSpinner view, int position, long id, Object item)
            {
                selectedRegion = item.toString();

                myRef.child("Admin").child("City").child(selectedRegion).addListenerForSingleValueEvent(new ValueEventListener()
                {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot snapshot)
                    {
                        arrySubRegion.clear();
                        for(DataSnapshot childSnapshot: snapshot.getChildren())
                        {
                            subregion = childSnapshot.getKey();
                            arrySubRegion.add(subregion);
                        }
                    }
                    @Override
                    public void onCancelled(@NonNull DatabaseError error)
                    {

                    }
                });

            }
        });

        materailSpinnerSubRegion.setItems(arrySubRegion);

        materailSpinnerRegion.setText(region_name);
        materailSpinnerSubRegion.setText(cityName);
        edtDate.setText(appDte);


        edtDate.setOnClickListener(this);
        btnNext.setOnClickListener(this);

    }

    @Override
    public void onClick(View v)
    {
        if(v == edtDate)
        {
            AppointmentDte();
        }
        else if(v == btnNext)
        {
            FeatchDetails();
        }
    }


    private void AppointmentDte()
    {
        Calendar c = Calendar.getInstance();
        int mYear = c.get(Calendar.YEAR); // current year
        int mMonth = c.get(Calendar.MONTH); // current month
        int mDay = c.get(Calendar.DAY_OF_MONTH); // current day

        DatePickerDialog datePickerDialog = new DatePickerDialog(RescheduleCityActivity.this, new DatePickerDialog.OnDateSetListener()
        {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth)
            {
                String monthString = String.valueOf(monthOfYear+1);
                if (monthString.length() == 1)
                {
                    monthString = "0" + monthString;
                }

                String dayString = String.valueOf(dayOfMonth);
                if (dayString.length() == 1)
                {
                    dayString = "0" + dayString;
                }
                edtDate.setText(year + "-" + (monthString) + "-" + dayString);
            }
        }, mYear, mMonth, mDay);


        // get 4 month before date
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.MONTH, 0);

        datePickerDialog.getDatePicker().setMinDate(calendar.getTimeInMillis()-1000);
        datePickerDialog.show();
    }

    private void FeatchDetails()
    {
        selectedRegion = materailSpinnerRegion.getText().toString();
        selectedSubregion = materailSpinnerSubRegion.getText().toString();
        dte = edtDate.getText().toString();

        if(selectedRegion.isEmpty())
        {
            new SweetAlertDialog(this, SweetAlertDialog.ERROR_TYPE)
                    .setTitleText("Error")
                    .setContentText("Selected Region")
                    .show();
        }
        else if(selectedSubregion.isEmpty())
        {
            new SweetAlertDialog(this, SweetAlertDialog.ERROR_TYPE)
                    .setTitleText("Error")
                    .setContentText("Selected SubRegion")
                    .show();
        }
        else if(dte.isEmpty())
        {
            new SweetAlertDialog(this, SweetAlertDialog.ERROR_TYPE)
                    .setTitleText("Error")
                    .setContentText("choose date")
                    .show();
        }
        else
        {
            newRegionName = materailSpinnerRegion.getText().toString();
            newCityName = materailSpinnerSubRegion.getText().toString();
            newDate = edtDate.getText().toString();

            if(newRegionName.equals(region_name))
            {
               flag1 = "1";
            }
            else
            {
                flag1 = "0";
            }

            if(newCityName.equals(cityName))
            {
                flag2 ="1";
            }
            else
            {
                flag2 = "0";
            }

             if(newDate.equals(appDte))
            {
                flag3 = "1";
            }
             else
             {
                 flag3 = "0";
             }


             Intent intent = new Intent(RescheduleCityActivity.this,RescheduleSlotActivity.class);
             intent.putExtra("region_name",newRegionName);
             intent.putExtra("cityName",newCityName);
             intent.putExtra("appDte",newDate);
             intent.putExtra("app_id",app_id);
             intent.putExtra("docID",docID);
             intent.putExtra("flag1",flag1);
             intent.putExtra("flag2",flag2);
             intent.putExtra("flag3",flag3);
             intent.putStringArrayListExtra("countAttendeesName", countAttendeesName);
             intent.putStringArrayListExtra("countAttendeesMobile",countAttendeesMobile);
             intent.putStringArrayListExtra("countAttendeesEmail",countAttendeesEmail);
             intent.putExtra("contact_person",contact_person);
             intent.putExtra("address",address);
             intent.putExtra("slot_id",slot_id);
             startActivity(intent);
        }
    }

}