package com.sanjay.service.bookappointment;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class AppointmentAdapter extends RecyclerView.Adapter<AppointmentAdapter.AppointmentViewHolder>
{

    Context context;
    AppointmentDetailsPojo appointmentDetailsPojo;

    String pPName,pPMobile,pPmail,execuName,execuName1,execuMobile,attendeename,attendeemobile,attendeemail,docStatus,contactperson,address,executionerDetails,bioDte,bioSTime,bioETime,docID;
    int currentItemPos;
    String id,currentDte;
    ProgressDialog progressDialog;
    BasicDetailsReq response = new BasicDetailsReq();
    String strResponsePost = "";

    public AppointmentAdapter(Context context, AppointmentDetailsPojo appointmentDetailsPojo)
    {
        this.context = context;
        this.appointmentDetailsPojo = appointmentDetailsPojo;
    }

    @Override
    public long getItemId(int pos) {
        return pos;
    }

    @NonNull
    @Override
    public AppointmentViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType)
    {
        View v;
        v = LayoutInflater.from(parent.getContext()).inflate(R.layout.appointment_item,parent,false);
        return new AppointmentAdapter.AppointmentViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull AppointmentViewHolder holder, int position)
    {


        docStatus = String.valueOf(appointmentDetailsPojo.getAppointments().get(position).getAppStatus());

        contactperson = appointmentDetailsPojo.getAppointments().get(position).getContactPerson();
        executionerDetails = appointmentDetailsPojo.getAppointments().get(position).getExecutionerId();
        docID = appointmentDetailsPojo.getAppointments().get(position).getMerchantRef();
        address = appointmentDetailsPojo.getAppointments().get(position).getAddress();



        bioDte = appointmentDetailsPojo.getAppointments().get(position).getAppointmentDate();
        bioSTime = appointmentDetailsPojo.getAppointments().get(position).getStartTime();
        bioETime = appointmentDetailsPojo.getAppointments().get(position).getEndTime();

        Log.wtf("contactperson",contactperson);

        String[] separated = contactperson.split(",");
        Log.wtf("separated", String.valueOf(separated));
        pPName = separated[0];
        Log.wtf("pPName",pPName);
        pPmail = separated[1];
        Log.wtf("pPmail",pPmail);
        pPMobile = separated[2];
        Log.wtf("pPMobile",pPMobile);



        if(executionerDetails!=null)
        {
            String[] separated1 = executionerDetails.split("Name :");
            execuName = separated1[1];

            String[] separated1_1 = execuName.split("Contact NO:");
            execuName1 = separated1_1[0];
            execuMobile = separated1_1[1];

            holder.txtExecuName.setText("Name : "+execuName1);
            holder.txtExecuMobile.setText("Mobile : "+execuMobile);
        }
        else
        {
            holder.txtExecuName.setText("No Executioner Details Available");
        }




        if(docStatus.equals("1"))
        {
            holder.txtDocStatus.setText("status: Requested");
        }
        else if(docStatus.equals("2"))
        {
            holder.txtDocStatus.setText("status: Confirmed");
        }
        else if(docStatus.equals("3"))
        {
            holder.txtDocStatus.setText("status: Reserved");
        }
        else if(docStatus.equals("4"))
        {
            holder.txtDocStatus.setText("status: Cancelled");
        }
        else if(docStatus.equals("5"))
        {
            holder.txtDocStatus.setText("status: Auto-Cancelled");
        }
        else if(docStatus.equals("6"))
        {
            holder.txtDocStatus.setText("status: Reschedule");
        }
        else if(docStatus.equals("7"))
        {
            holder.txtDocStatus.setText("status: Auto-Confirmed");
        }

        try
        {
            String _24HourTime = bioSTime;
            String _24HourETime = bioETime;
            SimpleDateFormat _24HourSDF = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
            SimpleDateFormat _12HourSDF = new SimpleDateFormat("hh:mm a");

            Date _24HourDt = _24HourSDF.parse(_24HourTime);
            bioSTime = _12HourSDF.format(_24HourDt);

            Date _24HourEDt = _24HourSDF.parse(_24HourETime);
            bioETime = _12HourSDF.format(_24HourEDt);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        holder.txtDocID.setText("DocID : "+docID);
        holder.txtPPName.setText("Name : "+pPName);

        holder.txtPPMail.setText("Mail : "+pPmail);
        holder.txtPPMobile.setText("Mobile : "+pPMobile);


        holder.txtBioDte.setText("Appointment Date : "+bioDte);
        holder.txtBioStartTime.setText("Appointment Start Time : "+bioSTime);
        holder.txtBioEndTime.setText("Appointment End Time : "+bioETime);
        holder.txtAddress.setText(address);


        currentItemPos = appointmentDetailsPojo.getAppointments().get(position).getAttendees().size();

        for(int i=0 ;i<currentItemPos;i++)
        {
            attendeemobile =  appointmentDetailsPojo.getAppointments().get(position).getAttendees().get(i).getContact();
            attendeename = appointmentDetailsPojo.getAppointments().get(position).getAttendees().get(i).getName();
            attendeemail = appointmentDetailsPojo.getAppointments().get(position).getAttendees().get(i).getEmail();


            Log.wtf("attendeename",attendeename);
            Log.wtf("attendeemobile",attendeemobile);

            TextView textViewName = new TextView(context);
            textViewName.setText("Name : "+attendeename);
            textViewName.layout(0,8,15,0);

            TextView textViewMobile = new TextView(context);
            textViewMobile.setText("Mobile : "+attendeemobile+"\n");
            textViewMobile.layout(15,10,15,0);

            TextView textViewMail = new TextView(context);
            textViewMail.setText("Mail : "+attendeemail+"\n");
            textViewMail.layout(15,10,15,0);

            holder.ll_attendes.addView(textViewName);
            holder.ll_attendes.addView(textViewMobile);
            holder.ll_attendes.addView(textViewMail);

        }


        holder.btnCancel.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                id = String.valueOf(appointmentDetailsPojo.getAppointments().get(position).getId());
                bioDte = appointmentDetailsPojo.getAppointments().get(position).getAppointmentDate();
                currentDte = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).format(new Date());

                try {
                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                    Date bioDate1 = sdf.parse(bioDte);
                    Date currentDate2 = sdf.parse(currentDte);

                    System.out.println(sdf.format(bioDate1));
                    System.out.println(sdf.format(currentDate2));

                    if (bioDate1.compareTo(currentDate2) > 0)
                    {
                       // System.out.println("Date1 is after Date2");

                        //then we will inflate the custom alert dialog xml that we created
                        View dialogView = LayoutInflater.from(context).inflate(R.layout.app_cancel_dialog, holder.viewGroup, false);

                        //Now we need an AlertDialog.Builder object
                        AlertDialog.Builder builder = new AlertDialog.Builder(context);

                        //setting the view of the builder to our custom view that we already inflated
                        builder.setView(dialogView);

                        //finally creating the alert dialog and displaying it
                        AlertDialog alertDialog = builder.create();
                        alertDialog.show();

                        Button btnYes = alertDialog.findViewById(R.id.buttonYes);
                        Button btnNo = alertDialog.findViewById(R.id.buttonNo);


                        btnNo.setOnClickListener(new Button.OnClickListener()
                        {
                            @Override
                            public void onClick(View v)
                            {
                                alertDialog.dismiss();
                            }
                        });

                        btnYes.setOnClickListener(new View.OnClickListener()
                        {
                            @Override
                            public void onClick(View v)
                            {
                                alertDialog.dismiss();

                                Intent i = new Intent(context,CancelAppointment.class);
                                i.putExtra("id",id);
                                i.putExtra("doc",docID);
                                context.startActivity(i);
                            }
                        });
                    }
                    else if (bioDate1.compareTo(currentDate2) < 0)
                    {
                       // System.out.println("Date1 is before Date2");
                        Toast.makeText(context,"you cannot cancel this appointment",Toast.LENGTH_LONG).show();

                    }
                    else if (bioDate1.compareTo(currentDate2) == 0)
                    {
                        Toast.makeText(context,"you cannot cancel today's appointment",Toast.LENGTH_LONG).show();
                    }
                    else
                     {
                        System.out.println("How to get here?");
                     }

                } catch (ParseException ex)
                {
                    ex.printStackTrace();
                }

            }
        });

        holder.btnRebook.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Appointment passAppointmentDte = appointmentDetailsPojo.getAppointments().get(position);
                Gson gson = new Gson();
                String myJson = gson.toJson(passAppointmentDte);

                Intent i = new Intent(context,RescheduleActivity.class);
                i.putExtra("myJson", myJson);
                context.startActivity(i);

            }
        });
    }

    @Override
    public int getItemCount()
    {
        return appointmentDetailsPojo.getAppointments().size();
    }

    public class AppointmentViewHolder extends RecyclerView.ViewHolder
    {
        TextView txtPPName,txtPPMobile,txtExecuName,txtExecuMobile,txtBioDte,txtBioStartTime,txtBioEndTime,txtPPMail,txtAddress;
        TextView txtDocID,txtDocStatus;
        Button btnCancel,btnRebook;
        View viewline;
        LinearLayout linearLayoutMain,ll_attendes;
        CardView cardView;
        ViewGroup viewGroup;


        public AppointmentViewHolder(@NonNull View itemView)
        {
            super(itemView);

            txtDocID = itemView.findViewById(R.id.txt_docID);
            txtDocStatus = itemView.findViewById(R.id.txt_docstatus);
            txtAddress = itemView.findViewById(R.id.txt_app_address);


            txtPPName = itemView.findViewById(R.id.txt_pname);
            txtPPMobile = itemView.findViewById(R.id.txt_pmobile);
            txtPPMail = itemView.findViewById(R.id.txt_pmail);

            txtExecuName = itemView.findViewById(R.id.txt_exeu_name);
            txtExecuMobile = itemView.findViewById(R.id.txt_exeu_mobile);
            txtBioDte = itemView.findViewById(R.id.txt_bdte);
            txtBioStartTime = itemView.findViewById(R.id.txt_bstime);
            txtBioEndTime = itemView.findViewById(R.id.txt_betime);

            viewline = itemView.findViewById(R.id.view_line4);
            linearLayoutMain = itemView.findViewById(R.id.ll_main);
            ll_attendes = itemView.findViewById(R.id.ll_attendes);
            cardView = itemView.findViewById(R.id.cardview_one);

            btnCancel = itemView.findViewById(R.id.btn_cancel);
            btnRebook = itemView.findViewById(R.id.btn_rebook);

            viewGroup = itemView.findViewById(android.R.id.content);
        }

    }

}
