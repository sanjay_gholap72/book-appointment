package com.sanjay.service.bookappointment;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.Gson;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import cn.pedant.SweetAlert.SweetAlertDialog;

public class RescheduleActivity extends AppCompatActivity implements  View.OnClickListener {
    String pPName,pPMobile,pPmail,attendeename,attendeemobile,attendeemail,docStatus,contactperson,executionerDetails,bioDte,bioSTime,bioETime,docID,address,appDte;
    int currentItemPos;

    Appointment ob;

    String strContactPersonName,strContactPersonMail,strContactPersonPhone,strContactPersonAddress;
    String tempAttendeesName,tempAttendeesEmail,tempAttendeesMobile;
    String region_name,slot_id,cityName,app_id;

    ArrayList<String> countAttendeesName,countAttendeesEmail,countAttendeesMobile;


    EditText edtPPName,edtPPMobile,edtPPMail;
    TextView txtDocID,txtDocStatus;
    View viewline;
    LinearLayout linearLayoutMain,ll_attendes;
    Button btnSave;

    String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
    String phonePattern = "^[+]?[0-9]{10,13}$";

    int flag = 0;

    ViewGroup viewGroup;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reschedule);

        Bundle bundle = getIntent().getExtras();
        Gson gson = new Gson();
        ob = gson.fromJson(getIntent().getStringExtra("myJson"),Appointment.class);

        txtDocID = findViewById(R.id.txt_docID);
        txtDocStatus = findViewById(R.id.txt_docstatus);

        edtPPName = findViewById(R.id.txt_pname);
        edtPPMobile = findViewById(R.id.txt_pmobile);
        edtPPMail = findViewById(R.id.txt_pmail);

        viewline = findViewById(R.id.view_line4);
        linearLayoutMain = findViewById(R.id.ll_main);
        ll_attendes = findViewById(R.id.ll_attendes);
        btnSave = findViewById(R.id.btn_bookapp);

        viewGroup = findViewById(android.R.id.content);
        docStatus = String.valueOf(ob.getAppStatus());

        contactperson = ob.getContactPerson();
        docID = ob.getMerchantRef();
        address = ob.getAddress();

        bioDte = ob.getAppointmentDate();
        bioSTime = ob.getStartTime();
        bioETime = ob.getEndTime();


        String[] separated = contactperson.split(",");
        pPName = separated[0];
        pPmail = separated[1];
        pPMobile = separated[2];

        countAttendeesName = new ArrayList<String>();
        countAttendeesEmail = new ArrayList<String>();
        countAttendeesMobile = new ArrayList<String>();



        if(docStatus.equals("1"))
        {
            txtDocStatus.setText("status: Requested");
        }
        else if(docStatus.equals("2"))
        {
            txtDocStatus.setText("status: Confirmed");
        }
        else if(docStatus.equals("3"))
        {
          txtDocStatus.setText("status: Reserved");
        }
        else if(docStatus.equals("4"))
        {
            txtDocStatus.setText("status: Cancelled");
        }
        else if(docStatus.equals("5"))
        {
            txtDocStatus.setText("status: Auto-Cancelled");
        }
        else if(docStatus.equals("6"))
        {
          txtDocStatus.setText("status: Reschedule");
        }
        else if(docStatus.equals("7"))
        {
            txtDocStatus.setText("status: Auto-Confirmed");
        }

        try
        {
            String _24HourTime = bioSTime;
            String _24HourETime = bioETime;
            SimpleDateFormat _24HourSDF = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
            SimpleDateFormat _12HourSDF = new SimpleDateFormat("hh:mm a");

            Date _24HourDt = _24HourSDF.parse(_24HourTime);
            bioSTime = _12HourSDF.format(_24HourDt);

            Date _24HourEDt = _24HourSDF.parse(_24HourETime);
            bioETime = _12HourSDF.format(_24HourEDt);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        txtDocID.setText("DocID : "+docID);
        edtPPName.setText(pPName);

        edtPPMail.setText(pPmail);
        edtPPMobile.setText(pPMobile);



        currentItemPos = ob.getAttendees().size();

        for(int i=0 ;i<currentItemPos;i++)
        {
            attendeemobile = ob.getAttendees().get(i).getContact();
            attendeename = ob.getAttendees().get(i).getName();
            attendeemail = ob.getAttendees().get(i).getEmail();

            TextView textView = new TextView(this);
            textView.setText("Attendee : "+(i+1));
            textView.layout(0,8,15,0);


            EditText edtViewName = new EditText(this);
            edtViewName.setHint("Attendee name");
            edtViewName.setText(attendeename);

            edtViewName.layout(0,8,15,0);

            EditText edtViewMobile = new EditText(this);
            edtViewMobile .setHint("Attendee mobile");
            edtViewMobile.setText(attendeemobile);
            edtViewMobile.layout(15,15,15,10);

            EditText edtViewMail = new EditText(this);
            edtViewMail .setHint("Attendee mail");
            edtViewMail.setText(attendeemail);
            edtViewMail.layout(15,15,15,10);

           ll_attendes.addView(textView);
           ll_attendes.addView(edtViewName);
           ll_attendes.addView(edtViewMobile);
           ll_attendes.addView(edtViewMail);

        }


        btnSave.setOnClickListener(this);
    }

    @Override
    public void onClick(View v)
    {
        if(v == btnSave)
        {
            saveData();
        }
    }

    private void saveData()
    {

        strContactPersonName = edtPPName.getText().toString().trim();
        strContactPersonMail = edtPPMail.getText().toString().trim();
        strContactPersonPhone = edtPPMobile.getText().toString().trim();

        strContactPersonAddress = address;


        if(TextUtils.isEmpty(strContactPersonName))
        {
            new SweetAlertDialog(this, SweetAlertDialog.ERROR_TYPE)
                    .setTitleText("Error")
                    .setContentText("Enter contact Person name")
                    .show();
        }
        else if(TextUtils.isEmpty(strContactPersonPhone))
        {
            new SweetAlertDialog(this, SweetAlertDialog.ERROR_TYPE)
                    .setTitleText("Error")
                    .setContentText("Enter contact Person Phone")
                    .show();
        }
        else if(!strContactPersonPhone.matches(phonePattern))
        {
            new SweetAlertDialog(this, SweetAlertDialog.ERROR_TYPE)
                    .setTitleText("Error")
                    .setContentText("Invalid Phone address")
                    .show();
        }

        else if(TextUtils.isEmpty(strContactPersonMail))
        {
            new SweetAlertDialog(this, SweetAlertDialog.ERROR_TYPE)
                    .setTitleText("Error")
                    .setContentText("Enter contact Person Mail")
                    .show();
        }
        else if(!strContactPersonMail.matches(emailPattern))
        {
            new SweetAlertDialog(this, SweetAlertDialog.ERROR_TYPE)
                    .setTitleText("Error")
                    .setContentText("Invalid email address")
                    .show();
        }
        else
        {
            if(currentItemPos != 0)
            {
                for(int i=0;i<currentItemPos;i++)
                {

                    tempAttendeesMobile = ob.getAttendees().get(i).getContact();
                    tempAttendeesName = ob.getAttendees().get(i).getName();
                    tempAttendeesEmail = ob.getAttendees().get(i).getEmail();

                    if(TextUtils.isEmpty(tempAttendeesName))
                    {
                        flag = 0;
                        new SweetAlertDialog(this, SweetAlertDialog.ERROR_TYPE)
                                .setTitleText("Error")
                                .setContentText("Enter attendess name")
                                .show();

                        break;
                    }
                    else if(TextUtils.isEmpty(tempAttendeesEmail))
                    {
                        flag = 0;
                        new SweetAlertDialog(this, SweetAlertDialog.ERROR_TYPE)
                                .setTitleText("Error")
                                .setContentText("Enter attendess email")
                                .show();
                        break;
                    }
                    else if(!tempAttendeesEmail.matches(emailPattern))
                    {
                        flag = 0;
                        new SweetAlertDialog(this, SweetAlertDialog.ERROR_TYPE)
                                .setTitleText("Error")
                                .setContentText("Invalid email address")
                                .show();
                        break;
                    }
                    else if(TextUtils.isEmpty(tempAttendeesMobile))
                    {
                        flag = 0;
                        new SweetAlertDialog(this, SweetAlertDialog.ERROR_TYPE)
                                .setTitleText("Error")
                                .setContentText("Enter attendess Mobile")
                                .show();
                        break;
                    }
                    else if(!tempAttendeesMobile.matches(phonePattern))
                    {
                        flag = 0;
                        new SweetAlertDialog(this, SweetAlertDialog.ERROR_TYPE)
                                .setTitleText("Error")
                                .setContentText("Please enter valided phone number")
                                .show();

                        break;
                    }
                    else
                    {
                        countAttendeesName.add (ob.getAttendees().get(i).getName());
                        countAttendeesMobile.add(ob.getAttendees().get(i).getContact());
                        countAttendeesEmail.add(ob.getAttendees().get(i).getEmail());
                        flag = 1;
                    }

                }
            }

            if(flag == 1)
            {
                region_name = ob.getRegionName();
                cityName = ob.getCityName();
                appDte = ob.getAppointmentDate();
                app_id = String.valueOf(ob.getId());

                Intent intent = new Intent(RescheduleActivity.this,RescheduleCityActivity.class);
                intent.putExtra("region_name",region_name);
                intent.putExtra("cityName",cityName);
                intent.putExtra("appDte",appDte);
                intent.putExtra("app_id",app_id);
                intent.putExtra("docID",docID);
                intent.putStringArrayListExtra("countAttendeesName", countAttendeesName);
                intent.putStringArrayListExtra("countAttendeesMobile",countAttendeesMobile);
                intent.putStringArrayListExtra("countAttendeesEmail",countAttendeesEmail);
                intent.putExtra("contact_person",(strContactPersonName+","+strContactPersonMail+","+strContactPersonPhone));
                intent.putExtra("address",address);
                intent.putExtra("slot_id",slot_id);
                startActivity(intent);

            }
        }
    }
}