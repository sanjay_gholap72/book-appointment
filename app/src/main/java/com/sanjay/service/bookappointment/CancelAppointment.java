package com.sanjay.service.bookappointment;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import static com.sanjay.service.bookappointment.BasicDetailsReq.getCancelAppointmrnt;
import static com.sanjay.service.bookappointment.BasicDetailsReq.get_auth_token;

public class CancelAppointment extends AppCompatActivity
{
    String id,docID;

    ProgressDialog progressDialog;
    BasicDetailsReq response;


    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cancel_appointment);

        Bundle bundle = getIntent().getExtras();
        id = bundle.getString("id");
        docID = bundle.getString("doc");


        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Loading");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setProgress(0);

        response = new BasicDetailsReq();

        CancelAppointmentDetails cancelAppointmentDetails = new CancelAppointmentDetails();
        cancelAppointmentDetails.execute();
    }

    private class CancelAppointmentDetails extends AsyncTask<String, String, String>
    {
        @Override
        protected void onPreExecute()
        {
            super.onPreExecute();
            progressDialog.show();
        }
        @Override
        protected String doInBackground(String... strings)
        {
            String strResponsePost = "";
            try
            {
                JSONObject jsonAttendees1 = new JSONObject();
                jsonAttendees1.put("app_id",id);
                jsonAttendees1.put("token",get_auth_token);


                JSONObject jsonObjectToken = new JSONObject();
                jsonObjectToken.put("token","DOtUBMhv5pk51tl0D37uBcezq85cXNN7hZQ7");

                JSONObject main = new JSONObject();
                main.put("appointment",jsonAttendees1);
                main.put("auth_token",jsonObjectToken);

                String json = "";
                json = main.toString();

                strResponsePost = response.doPostRequest(getCancelAppointmrnt, json);


            } catch (JSONException | IOException e)
            {
                e.printStackTrace();
            }

            return strResponsePost;

        }


        @Override
        protected void onPostExecute(String result)
        {
            Log.wtf("result",result);

            if(result!=null)
            {
                progressDialog.dismiss();
                try
                {
                    JSONObject jsonObject = new JSONObject(result);
                    String jStrResult = jsonObject.getString("status");
                    if (jStrResult.equals("1"))
                    {
                        String jStrResultMeg = jsonObject.getString("msg");

                        if(jStrResultMeg.equals("Success"))
                        {
                            Toast.makeText(CancelAppointment.this,"Appointment cancel successfully",Toast.LENGTH_LONG).show();
                            Intent intent = new Intent(CancelAppointment.this,MainActivity.class);
                            startActivity(intent);
                        }
                        else
                        {
                            Toast.makeText(CancelAppointment.this,"Appointment cannot cancel ",Toast.LENGTH_LONG).show();
                            Intent intent = new Intent(CancelAppointment.this,MainActivity.class);
                            startActivity(intent);
                        }
                    }
                    else if (jStrResult.equals("0"))
                    {
                        Toast.makeText(CancelAppointment.this,"Appointment cannot cancel ",Toast.LENGTH_LONG).show();
                        Intent intent = new Intent(CancelAppointment.this,MainActivity.class);
                        startActivity(intent);
                    }

                }
                catch (JSONException e)
                {
                    e.printStackTrace();
                }

            }

        }
    }
}