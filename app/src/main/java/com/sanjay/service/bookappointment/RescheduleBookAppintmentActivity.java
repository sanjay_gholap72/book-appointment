package com.sanjay.service.bookappointment;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import cn.pedant.SweetAlert.SweetAlertDialog;

import static com.sanjay.service.bookappointment.BasicDetailsReq.APPOINTMENTURL;
import static com.sanjay.service.bookappointment.BasicDetailsReq.getReschedule;
import static com.sanjay.service.bookappointment.BasicDetailsReq.get_auth_token;

public class RescheduleBookAppintmentActivity extends AppCompatActivity implements View.OnClickListener
{
    String region_name,slot_id,cityName,app_id,appDte,docID,contact_person,address,strContactPersonAddress;
    String flag;

    int count = 0;

    TextView txt_docID;
    EditText edtAddress;

    ArrayList<String> countAttendeesName,countAttendeesEmail,countAttendeesMobile;
    Button btnNext;

    ProgressDialog progressDialog;
    BasicDetailsReq response;



    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reschedule_book_appintment);

        Bundle bundle = getIntent().getExtras();
        countAttendeesName = bundle.getStringArrayList("countAttendeesName");
        countAttendeesEmail = bundle.getStringArrayList("countAttendeesEmail");
        countAttendeesMobile = bundle.getStringArrayList("countAttendeesMobile");
        region_name = bundle.getString("region_name");
        cityName = bundle.getString("cityName");
        appDte = bundle.getString("appDte");
        app_id = bundle.getString("app_id");
        docID = bundle.getString("docID");
        contact_person = bundle.getString("contact_person");
        flag = bundle.getString("flag");
        address = bundle.getString("address");
        slot_id = bundle.getString("slot_id");

        Log.wtf("region_name",region_name);
        Log.wtf("cityName",cityName);
        Log.wtf("appDte",appDte);
        Log.wtf("app_id",app_id);
        Log.wtf("contact_person",contact_person);
        Log.wtf("slot_id",slot_id);
        Log.wtf("countAttendeesMobile", String.valueOf(countAttendeesMobile));

        count = countAttendeesName.size();

        for(int i=0;i<count;i++)
        {

            Log.wtf("countAttendeesName",countAttendeesName.get(i));
            Log.wtf("countAttendeesEmail",countAttendeesEmail.get(i));
            Log.wtf("countAttendeesMobile",countAttendeesMobile.get(i));
        }

        txt_docID = findViewById(R.id.txt_docID);
        txt_docID.setText("DocID : "+docID);

        edtAddress = findViewById(R.id.edt_contactaddress);
        btnNext = findViewById(R.id.btn_next);

        edtAddress.setText(address);

        response = new BasicDetailsReq();


        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Loading");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setProgress(0);


        btnNext.setOnClickListener(this);
    }

    @Override
    public void onClick(View v)
    {
       if(v == btnNext)
       {
           RescheduleAppointment();
       }
    }

    private void RescheduleAppointment()
    {
        strContactPersonAddress = edtAddress.getText().toString().trim();

        if(TextUtils.isEmpty(strContactPersonAddress))
        {
            new SweetAlertDialog(this, SweetAlertDialog.ERROR_TYPE)
                    .setTitleText("Error")
                    .setContentText("Enter  Biometric Address")
                    .show();
        }
        else
        {
            progressDialog.show();
            FeatchAppointmentDetails featchAppointmentDetails = new FeatchAppointmentDetails();
            featchAppointmentDetails.execute();
        }
    }


    private class FeatchAppointmentDetails extends AsyncTask<String, String, String>
    {
        JSONObject jsonAppointmentdtl;

        @Override
        protected void onPreExecute()
        {
            super.onPreExecute();
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... strings)
        {
            String strResponsePost = "";
            JSONArray jsonArrayPerson = new JSONArray();
            try
            {

                if(count!=0)
                {
                    Log.wtf("count", String.valueOf(count));
                    for(int j = 0 ;j<count ;j++)
                    {

                        JSONObject  jsonAttendees = new JSONObject();
                        jsonAttendees.put("name",countAttendeesName.get(j));
                        jsonAttendees.put("email",countAttendeesEmail.get(j));
                        jsonAttendees.put("contact",countAttendeesMobile.get(j));

                        Log.wtf("countAttendeesName", String.valueOf(jsonAttendees));

                        jsonArrayPerson.put(jsonAttendees);
                    }

                }

                if(TextUtils.isEmpty(slot_id))
                {
                    jsonAppointmentdtl= new JSONObject();
                    jsonAppointmentdtl.put("app_id",app_id);
                    jsonAppointmentdtl.put("request_no",docID);
                    jsonAppointmentdtl.put("city",region_name);
                    jsonAppointmentdtl.put("region_name",cityName);
                    jsonAppointmentdtl.put("address",strContactPersonAddress);
                    jsonAppointmentdtl.put("contact_person",contact_person);
                    jsonAppointmentdtl.put("app_flag","2");
                    jsonAppointmentdtl.put("attendees",jsonArrayPerson);
                    jsonAppointmentdtl.put("app_date",appDte);
                }
                else
                {
                     jsonAppointmentdtl= new JSONObject();
                     jsonAppointmentdtl.put("app_id",app_id);
                    jsonAppointmentdtl.put("request_no",docID);
                    jsonAppointmentdtl.put("city",region_name);
                    jsonAppointmentdtl.put("region_name",cityName);
                    jsonAppointmentdtl.put("slot_id",slot_id);
                    jsonAppointmentdtl.put("address",strContactPersonAddress);
                    jsonAppointmentdtl.put("contact_person",contact_person);
                    jsonAppointmentdtl.put("app_flag","2");
                    jsonAppointmentdtl.put("attendees",jsonArrayPerson);
                    jsonAppointmentdtl.put("app_date",appDte);
                }

                JSONObject jsonObjectToken = new JSONObject();
                jsonObjectToken.put("token",get_auth_token);

                JSONObject main = new JSONObject();
                main.put("appointment",jsonAppointmentdtl);
                main.put("auth_token",jsonObjectToken);

                String json = "";
                json = main.toString();

                strResponsePost = response.doPostRequest(getReschedule, json);


            } catch (JSONException | IOException e)
            {
                e.printStackTrace();
            }

            return strResponsePost;
        }

        @Override
        protected void onPostExecute(String result)
        {
            Log.wtf("result",result);

            if(result.equals(null) || result.equals(""))
            {
                new SweetAlertDialog(RescheduleBookAppintmentActivity.this, SweetAlertDialog.ERROR_TYPE)
                        .setTitleText("Error")
                        .setContentText("Something went wrong please try again ")
                        .show();
            }
            else
            {
                progressDialog.dismiss();
                try
                {
                    JSONObject jsonObject = new JSONObject(result);
                    String jStrResult = jsonObject.getString("status");

                    if(jStrResult.equals("1"))
                    {
                        Toast.makeText(RescheduleBookAppintmentActivity.this,"Please wait for approved",Toast.LENGTH_LONG).show();
                        Intent intent = new Intent(RescheduleBookAppintmentActivity.this,MainActivity.class);
                        startActivity(intent);

                    }
                    else if(jStrResult.equals("0"))
                    {
                        new SweetAlertDialog(RescheduleBookAppintmentActivity.this, SweetAlertDialog.ERROR_TYPE)
                                .setTitleText("Error")
                                .setContentText("Please try again")
                                .show();
                    }

                }
                catch (JSONException e)
                {
                    e.printStackTrace();
                }

            }

        }
    }
}
