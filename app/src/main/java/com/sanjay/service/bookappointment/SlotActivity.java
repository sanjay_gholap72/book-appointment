package com.sanjay.service.bookappointment;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.json.JSONObject;

import java.util.Calendar;

import cn.pedant.SweetAlert.SweetAlertDialog;

import static com.sanjay.service.bookappointment.BasicDetailsReq.getTimeSlot;
import static com.sanjay.service.bookappointment.BasicDetailsReq.get_auth_token;
import static com.sanjay.service.bookappointment.BasicDetailsReq.mID;


public class SlotActivity extends AppCompatActivity implements View.OnClickListener
{
    String selectedRegion,selectedSubregion,selectedValue,dte,region_id,slotID,docID;
    String startTime,endTime;
    ProgressDialog progressDialog;
    int i,k=0,sizeTemp=0,tempcol;

    DatabaseReference myRef;
    BasicDetailsReq basicDetailsReq;
    TimeSlotPojo timeSlotPojos;

    TableLayout tableLayout;
    TextView txtDate;
    TableRow tr;


    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_slot);

        Bundle bundle = getIntent().getExtras();
        selectedRegion = bundle.getString("selectedRegion");
        selectedSubregion = bundle.getString("selectedSubregion");
        dte = bundle.getString("dte");
        docID = bundle.getString("docID");

        myRef = FirebaseDatabase.getInstance().getReference();
        basicDetailsReq = new BasicDetailsReq();

        tableLayout = findViewById(R.id.table_main);
        txtDate = findViewById(R.id.txt_dte);

        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Loading");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setProgress(0);

        txtDate.setText(dte);

        txtDate.setOnClickListener(this);

        Fachdata();

    }

    @Override
    public void onClick(View v)
    {
      if(v == txtDate)
      {
          AppointmentDte();
      }
    }

    private void AppointmentDte()
    {
        Calendar c = Calendar.getInstance();
        int mYear = c.get(Calendar.YEAR); // current year
        int mMonth = c.get(Calendar.MONTH); // current month
        int mDay = c.get(Calendar.DAY_OF_MONTH); // current day

        DatePickerDialog datePickerDialog = new DatePickerDialog(SlotActivity.this, new DatePickerDialog.OnDateSetListener()
        {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth)
            {
                String monthString = String.valueOf(monthOfYear+1);
                if (monthString.length() == 1)
                {
                    monthString = "0" + monthString;
                }

                String dayString = String.valueOf(dayOfMonth);
                if (dayString.length() == 1)
                {
                    dayString = "0" + dayString;
                }
                txtDate.setText(year + "-" + (monthString) + "-" + dayString);
                dte = txtDate.getText().toString();
                Fachdata();
            }
        }, mYear, mMonth, mDay);


        // get 4 month before date
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.MONTH, 0);

        datePickerDialog.getDatePicker().setMinDate(calendar.getTimeInMillis());
        datePickerDialog.show();
    }

    private void Fachdata()
    {
       progressDialog.show();

        myRef.child("Admin").child("City").child(selectedRegion).addListenerForSingleValueEvent(new ValueEventListener()
            {
                @Override
                public void onDataChange(@NonNull DataSnapshot snapshot)
                {
                    selectedValue = String.valueOf(snapshot.child(selectedSubregion).getValue());
                    Log.wtf("  selectedValue",  selectedValue);
                    GetSelctedValue(selectedValue);
                }

                @Override
                public void onCancelled(@NonNull DatabaseError error)
                {

                }
            });
    }

    private void GetSelctedValue(String selectedValue)
    {
        region_id = selectedValue;
        FeatchAppointmentDetails featchAppointmentDetails = new FeatchAppointmentDetails();
        featchAppointmentDetails.execute();
        Log.wtf("region_id ",region_id );
    }

    private class FeatchAppointmentDetails extends AsyncTask<String, String, String>
    {

        @Override
        protected void onPreExecute()
        {
            super.onPreExecute();
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... strings)
        {

            String strResponsePost = "";
             sizeTemp = 0;
             k = 0;

            try
            {
                String url = getTimeSlot+"region_id="+region_id+"&app_date="+dte+"&merchant_ref_name="+mID+"&get_auth_token="+get_auth_token;
                Log.wtf("url",url);
                strResponsePost = basicDetailsReq.doGetRequest(url);
                Log.wtf("strResponsePost",strResponsePost);


            }
            catch (Exception e)
            {
                Log.wtf("e",e);
            }

            return strResponsePost;
        }

        @Override
        protected void onPostExecute(String result)
        {

            if(result==null || result.equals(""))
            {
                new SweetAlertDialog(SlotActivity.this, SweetAlertDialog.ERROR_TYPE)
                        .setTitleText("Error")
                        .setContentText("Something went wrong please try again ")
                        .show();
            }
            else
            {
                super.onPostExecute(result);
                try
                {
                    progressDialog.dismiss();
                    JSONObject jsonObject = new JSONObject(result);
                    String jStrResult = jsonObject.getString("status");

                    Log.wtf("jstrResult",jStrResult);
                    if(jStrResult.equals("OK"))
                    {
                        String temp = result;
                        GsonBuilder gsonBuilder = new GsonBuilder();
                        Gson gson = gsonBuilder.create();

                        timeSlotPojos = gson.fromJson(temp,TimeSlotPojo.class);

                         sizeTemp = timeSlotPojos.getTimeSlot().size();

                        tableLayout.removeAllViews();
                        for(i= 0;i<sizeTemp;i++)
                        {
                            Log.wtf("i", String.valueOf(i));
                            startTime = timeSlotPojos.getTimeSlot().get(i).getcStartTime();
                            endTime = timeSlotPojos.getTimeSlot().get(i).getcEndTime();
                            slotID = timeSlotPojos.getTimeSlot().get(i).getSlotId();

                            tableLayout.setStretchAllColumns(true);
                            tableLayout.bringToFront();
                            tr =  new TableRow(SlotActivity.this);

                            tempcol = 2;

                            for(int j = 0; j < tempcol; j++)
                            {
                                    Log.wtf("j", String.valueOf(j));
                                    Log.wtf("k", String.valueOf(k));

                                    Button button = new Button(SlotActivity.this);
                                    button.setId(Integer.parseInt(timeSlotPojos.getTimeSlot().get(k).getSlotId()));
                                    button.setHeight(30);
                                    button.setWidth(30);
                                    button.setText(timeSlotPojos.getTimeSlot().get(k).getcStartTime()+" - "+timeSlotPojos.getTimeSlot().get(k).getcEndTime());
                                    button.setTextSize(12);

                                    button.setOnClickListener(new View.OnClickListener()
                                    {
                                        @Override
                                        public void onClick(View v)
                                        {

                                         String textbutton = String.valueOf(button.getText());
                                         slotID = String.valueOf(button.getId());

                                           Intent intent = new Intent(SlotActivity.this,BookAppintmentActivity.class);

                                           intent.putExtra("slotID",slotID);
                                           intent.putExtra("region",selectedRegion);
                                           intent.putExtra("subregion",selectedSubregion);
                                           intent.putExtra("txt",textbutton);
                                           intent.putExtra("dte",dte);
                                           intent.putExtra("docID",docID);
                                           startActivity(intent);


                                        }
                                    });

                                    tr.addView(button);
                                    /*txtGeneric.setHeight(30); txtGeneric.setWidth(50);   txtGeneric.setTextColor(Color.BLUE);*/

                                if(k != sizeTemp)
                                {
                                    k = k+1;
                                }



                            }
                                tableLayout.addView(tr);
                        }
                    }

                }
                catch (Exception e)
                {
                    Log.wtf("e",e);
                }
            }
        }
    }


}