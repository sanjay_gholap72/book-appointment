package com.sanjay.service.bookappointment;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class AttendeeAdapter extends RecyclerView.Adapter<AttendeeAdapter.AttendeeViewHolder>
{
    private Context mContext;
    private List<AttendeesList> mDataSet;
    private String name,mobile;

    public AttendeeAdapter(Context mContext, List<AttendeesList> mDataSet)
    {
        this.mContext = mContext;
        this.mDataSet = mDataSet;
    }

    @NonNull
    @Override
    public AttendeeViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType)
    {
        View v;
        v = LayoutInflater.from(parent.getContext()).inflate(R.layout.attendees_item,parent,false);
        return new AttendeeViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull AttendeeViewHolder holder, int position)
    {
         AttendeesList attendeesList = mDataSet.get(position);
         name = attendeesList.getName();
         mobile = attendeesList.getMobile();

         holder.txtName.setText("Name : "+name);
         holder.txtMobile.setText("Mobile : "+mobile);
    }

    @Override
    public int getItemCount()
    {
        return mDataSet.size();
    }


    public class AttendeeViewHolder extends RecyclerView.ViewHolder
    {
        TextView txtName,txtMobile;

        public AttendeeViewHolder(@NonNull View itemView)
        {
            super(itemView);

            txtName = itemView.findViewById(R.id.txt_name);
            txtMobile = itemView.findViewById(R.id.txt_mobile);
        }
    }

}
