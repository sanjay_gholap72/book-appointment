
package com.sanjay.service.bookappointment;

import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Appointment {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("merchant_ref")
    @Expose
    private String merchantRef;
    @SerializedName("event_id")
    @Expose
    private Object eventId;
    @SerializedName("calendar_id")
    @Expose
    private Integer calendarId;
    @SerializedName("address")
    @Expose
    private String address;
    @SerializedName("attendees")
    @Expose
    private List<Attendee> attendees = null;
    @SerializedName("approved")
    @Expose
    private Boolean approved;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;
    @SerializedName("region_id")
    @Expose
    private Integer regionId;
    @SerializedName("users_id")
    @Expose
    private Object usersId;
    @SerializedName("executioner_id")
    @Expose
    private String executionerId;
    @SerializedName("merchant_id")
    @Expose
    private Object merchantId;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("color_id")
    @Expose
    private Object colorId;
    @SerializedName("city_id")
    @Expose
    private Integer cityId;
    @SerializedName("division_id")
    @Expose
    private Integer divisionId;
    @SerializedName("latitude")
    @Expose
    private String latitude;
    @SerializedName("longitude")
    @Expose
    private String longitude;
    @SerializedName("logical_del")
    @Expose
    private Boolean logicalDel;
    @SerializedName("discount")
    @Expose
    private Boolean discount;
    @SerializedName("app_status")
    @Expose
    private Integer appStatus;
    @SerializedName("landmark")
    @Expose
    private String landmark;
    @SerializedName("contact_person")
    @Expose
    private String contactPerson;
    @SerializedName("app_type")
    @Expose
    private Object appType;
    @SerializedName("is_remote")
    @Expose
    private Boolean isRemote;
    @SerializedName("cust_type")
    @Expose
    private Integer custType;
    @SerializedName("is_free")
    @Expose
    private Boolean isFree;
    @SerializedName("free_reason")
    @Expose
    private Object freeReason;
    @SerializedName("long_dist")
    @Expose
    private Boolean longDist;
    @SerializedName("appt_executioner_charges")
    @Expose
    private Integer apptExecutionerCharges;
    @SerializedName("only_tech_support")
    @Expose
    private Object onlyTechSupport;
    @SerializedName("start_time")
    @Expose
    private String startTime;
    @SerializedName("end_time")
    @Expose
    private String endTime;
    @SerializedName("appointment_date")
    @Expose
    private String appointmentDate;
    @SerializedName("region_name")
    @Expose
    private String regionName;
    @SerializedName("city_name")
    @Expose
    private String cityName;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getMerchantRef() {
        return merchantRef;
    }

    public void setMerchantRef(String merchantRef) {
        this.merchantRef = merchantRef;
    }

    public Object getEventId() {
        return eventId;
    }

    public void setEventId(Object eventId) {
        this.eventId = eventId;
    }

    public Integer getCalendarId() {
        return calendarId;
    }

    public void setCalendarId(Integer calendarId) {
        this.calendarId = calendarId;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public List<Attendee> getAttendees() {
        return attendees;
    }

    public void setAttendees(List<Attendee> attendees) {
        this.attendees = attendees;
    }

    public Boolean getApproved() {
        return approved;
    }

    public void setApproved(Boolean approved) {
        this.approved = approved;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Integer getRegionId() {
        return regionId;
    }

    public void setRegionId(Integer regionId) {
        this.regionId = regionId;
    }

    public Object getUsersId() {
        return usersId;
    }

    public void setUsersId(Object usersId) {
        this.usersId = usersId;
    }

    public String getExecutionerId() {
        return executionerId;
    }

    public void setExecutionerId(String executionerId) {
        this.executionerId = executionerId;
    }

    public Object getMerchantId() {
        return merchantId;
    }

    public void setMerchantId(Object merchantId) {
        this.merchantId = merchantId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Object getColorId() {
        return colorId;
    }

    public void setColorId(Object colorId) {
        this.colorId = colorId;
    }

    public Integer getCityId() {
        return cityId;
    }

    public void setCityId(Integer cityId) {
        this.cityId = cityId;
    }

    public Integer getDivisionId() {
        return divisionId;
    }

    public void setDivisionId(Integer divisionId) {
        this.divisionId = divisionId;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public Boolean getLogicalDel() {
        return logicalDel;
    }

    public void setLogicalDel(Boolean logicalDel) {
        this.logicalDel = logicalDel;
    }

    public Boolean getDiscount() {
        return discount;
    }

    public void setDiscount(Boolean discount) {
        this.discount = discount;
    }

    public Integer getAppStatus() {
        return appStatus;
    }

    public void setAppStatus(Integer appStatus) {
        this.appStatus = appStatus;
    }

    public String getLandmark() {
        return landmark;
    }

    public void setLandmark(String landmark) {
        this.landmark = landmark;
    }

    public String getContactPerson() {
        return contactPerson;
    }

    public void setContactPerson(String contactPerson) {
        this.contactPerson = contactPerson;
    }

    public Object getAppType() {
        return appType;
    }

    public void setAppType(Object appType) {
        this.appType = appType;
    }

    public Boolean getIsRemote() {
        return isRemote;
    }

    public void setIsRemote(Boolean isRemote) {
        this.isRemote = isRemote;
    }

    public Integer getCustType() {
        return custType;
    }

    public void setCustType(Integer custType) {
        this.custType = custType;
    }

    public Boolean getIsFree() {
        return isFree;
    }

    public void setIsFree(Boolean isFree) {
        this.isFree = isFree;
    }

    public Object getFreeReason() {
        return freeReason;
    }

    public void setFreeReason(Object freeReason) {
        this.freeReason = freeReason;
    }

    public Boolean getLongDist() {
        return longDist;
    }

    public void setLongDist(Boolean longDist) {
        this.longDist = longDist;
    }

    public Integer getApptExecutionerCharges() {
        return apptExecutionerCharges;
    }

    public void setApptExecutionerCharges(Integer apptExecutionerCharges) {
        this.apptExecutionerCharges = apptExecutionerCharges;
    }

    public Object getOnlyTechSupport() {
        return onlyTechSupport;
    }

    public void setOnlyTechSupport(Object onlyTechSupport) {
        this.onlyTechSupport = onlyTechSupport;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public String getAppointmentDate() {
        return appointmentDate;
    }

    public void setAppointmentDate(String appointmentDate) {
        this.appointmentDate = appointmentDate;
    }

    public Boolean getRemote() {
        return isRemote;
    }

    public void setRemote(Boolean remote) {
        isRemote = remote;
    }

    public String getRegionName() {
        return regionName;
    }

    public void setRegionName(String regionName) {
        this.regionName = regionName;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }
}
