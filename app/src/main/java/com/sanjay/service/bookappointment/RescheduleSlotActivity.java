package com.sanjay.service.bookappointment;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.json.JSONObject;

import java.util.ArrayList;

import cn.pedant.SweetAlert.SweetAlertDialog;

import static com.sanjay.service.bookappointment.BasicDetailsReq.getTimeSlot;
import static com.sanjay.service.bookappointment.BasicDetailsReq.get_auth_token;
import static com.sanjay.service.bookappointment.BasicDetailsReq.mID;

public class RescheduleSlotActivity extends AppCompatActivity implements View.OnClickListener
{
    String region_name,slot_id,cityName,app_id,appDte,docID,contact_person,address;
    String flag1,flag2,flag3;

    ArrayList<String> countAttendeesName,countAttendeesEmail,countAttendeesMobile;
    Button btnNext;

    String selectedValue,region_id,slotID;
    String startTime,endTime;
    ProgressDialog progressDialog;
    int i,k=0,sizeTemp=0,tempcol;

    DatabaseReference myRef;
    BasicDetailsReq basicDetailsReq;
    TimeSlotPojo timeSlotPojos;

    TableLayout tableLayout;

    TableRow tr;


    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reschedule_slot);

        Bundle bundle = getIntent().getExtras();
        countAttendeesName = bundle.getStringArrayList("countAttendeesName");
        countAttendeesEmail = bundle.getStringArrayList("countAttendeesEmail");
        countAttendeesMobile = bundle.getStringArrayList("countAttendeesMobile");
        region_name = bundle.getString("region_name");
        cityName = bundle.getString("cityName");
        appDte = bundle.getString("appDte");
        app_id = bundle.getString("app_id");
        docID = bundle.getString("docID");
        contact_person = bundle.getString("contact_person");
        flag1 = bundle.getString("flag1");
        flag2 = bundle.getString("flag2");
        flag3 = bundle.getString("flag3");
        address = bundle.getString("address");
        slot_id = bundle.getString("slot_id");

        myRef = FirebaseDatabase.getInstance().getReference();
        basicDetailsReq = new BasicDetailsReq();

        tableLayout = findViewById(R.id.table_main);
        btnNext = findViewById(R.id.btn_next);


        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Loading");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setProgress(0);

        Fachdata();
        btnNext.setOnClickListener(this);
    }


    private void Fachdata()
    {
        progressDialog.show();

        myRef.child("Admin").child("City").child(region_name).addListenerForSingleValueEvent(new ValueEventListener()
        {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot)
            {
                selectedValue = String.valueOf(snapshot.child(cityName).getValue());
                Log.wtf("  selectedValue",  selectedValue);
                GetSelctedValue(selectedValue);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error)
            {

            }
        });
    }

    private void GetSelctedValue(String selectedValue)
    {
        region_id = selectedValue;
        FeatchAppointmentDetails featchAppointmentDetails = new FeatchAppointmentDetails();
        featchAppointmentDetails.execute();
        Log.wtf("region_id ",region_id );
    }

    @Override
    public void onClick(View v)
    {
       if(v == btnNext)
       {
           PassData();
       }
    }




    private class FeatchAppointmentDetails extends AsyncTask<String, String, String>
    {

        @Override
        protected void onPreExecute()
        {
            super.onPreExecute();
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... strings)
        {

            String strResponsePost = "";
            sizeTemp = 0;
            k = 0;

            try
            {
                String url = getTimeSlot+"region_id="+region_id+"&app_date="+appDte+"&merchant_ref_name="+mID+"&get_auth_token="+get_auth_token;
                Log.wtf("url",url);
                strResponsePost = basicDetailsReq.doGetRequest(url);
                Log.wtf("strResponsePost",strResponsePost);


            }
            catch (Exception e)
            {
                Log.wtf("e",e);
            }

            return strResponsePost;
        }

        @Override
        protected void onPostExecute(String result)
        {

            if(result==null || result.equals(""))
            {
                new SweetAlertDialog(RescheduleSlotActivity.this, SweetAlertDialog.ERROR_TYPE)
                        .setTitleText("Error")
                        .setContentText("Something went wrong please try again ")
                        .show();
            }
            else
            {
                super.onPostExecute(result);
                try
                {
                    progressDialog.dismiss();
                    JSONObject jsonObject = new JSONObject(result);
                    String jStrResult = jsonObject.getString("status");

                    Log.wtf("jstrResult",jStrResult);
                    if(jStrResult.equals("OK"))
                    {
                        String temp = result;
                        GsonBuilder gsonBuilder = new GsonBuilder();
                        Gson gson = gsonBuilder.create();

                        timeSlotPojos = gson.fromJson(temp,TimeSlotPojo.class);

                        sizeTemp = timeSlotPojos.getTimeSlot().size();

                        tableLayout.removeAllViews();
                        for(i= 0;i<sizeTemp;i++)
                        {
                            Log.wtf("i", String.valueOf(i));
                            startTime = timeSlotPojos.getTimeSlot().get(i).getcStartTime();
                            endTime = timeSlotPojos.getTimeSlot().get(i).getcEndTime();
                            slotID = timeSlotPojos.getTimeSlot().get(i).getSlotId();

                            tableLayout.setStretchAllColumns(true);
                            tableLayout.bringToFront();
                            tr =  new TableRow(RescheduleSlotActivity.this);

                            tempcol = 2;

                            for(int j = 0; j < tempcol; j++)
                            {


                                Button button = new Button(RescheduleSlotActivity.this);
                                button.setId(Integer.parseInt(timeSlotPojos.getTimeSlot().get(k).getSlotId()));
                                button.setHeight(30);
                                button.setWidth(30);
                                button.setText(timeSlotPojos.getTimeSlot().get(k).getcStartTime()+" - "+timeSlotPojos.getTimeSlot().get(k).getcEndTime());
                                button.setTextSize(12);

                                button.setOnClickListener(new View.OnClickListener()
                                {
                                    @SuppressLint("ResourceAsColor")
                                    @Override
                                    public void onClick(View v)
                                    {

                                        slot_id = String.valueOf(button.getId());
                                        button.setBackgroundColor(R.color.slotbutton);
                                        button.setTextColor(R.color.white);
                                    }
                                });

                                tr.addView(button);
                                /*txtGeneric.setHeight(30); txtGeneric.setWidth(50);   txtGeneric.setTextColor(Color.BLUE);*/

                                if(k != sizeTemp)
                                {
                                    k = k+1;
                                }



                            }
                            tableLayout.addView(tr);
                        }
                    }

                }
                catch (Exception e)
                {
                    Log.wtf("e",e);
                }
            }
        }
    }


    private void PassData()
    {
        if(flag1.equals("0") || flag2.equals("0") || flag3.equals("0"))
        {
            if(TextUtils.isEmpty(slot_id))
            {
                new SweetAlertDialog(this, SweetAlertDialog.ERROR_TYPE)
                        .setTitleText("Error")
                        .setContentText("Select slot")
                        .show();
            }
            else
            {
                AddtoNext();
            }
        }
        else
        {
            AddtoNext();
        }
    }

    private void AddtoNext()
    {
        Intent intent = new Intent(RescheduleSlotActivity.this,RescheduleBookAppintmentActivity.class);
        intent.putExtra("region_name",region_name);
        intent.putExtra("cityName",cityName);
        intent.putExtra("appDte",appDte);
        intent.putExtra("app_id",app_id);
        intent.putExtra("docID",docID);
        intent.putExtra("flag1",flag1);
        intent.putExtra("slot_id",slot_id);
        intent.putStringArrayListExtra("countAttendeesName", countAttendeesName);
        intent.putStringArrayListExtra("countAttendeesMobile",countAttendeesMobile);
        intent.putStringArrayListExtra("countAttendeesEmail",countAttendeesEmail);
        intent.putExtra("contact_person",contact_person);
        intent.putExtra("slot_id",slot_id);
        intent.putExtra("address",address);
        startActivity(intent);

    }

}